﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    // Layers
    public static readonly int LayerGround              = LayerMask.NameToLayer("Ground");
    public static readonly int LayerWall                = LayerMask.NameToLayer("Wall");
    public static readonly int LayerCollectable         = LayerMask.NameToLayer("Collectable");
    public static readonly int LayerHazard              = LayerMask.NameToLayer("Hazard");
    public static readonly int LayerGoal                = LayerMask.NameToLayer("Goal");

    // Tags
    public static readonly string TheContainerTag       = "TheContainer";
    public static readonly string PlayerTag             = "Player";
    public static readonly string ThrowArrowTag         = "ThrowArrow";

    // Input Actions
    public static readonly string JumpAction            = "Jump";
    public static readonly string ThrowAction           = "Throw";
    public static readonly string DashAction            = "Dash";
    public static readonly string ResetAction           = "Reset";
    public static readonly string CancelAction          = "Cancel2";
    public static readonly string RS_HAxis              = "HorizontalRightStick";
    public static readonly string RS_VAxis              = "VerticalRightStick";
    public static readonly string MouseXAxis            = "MouseX";
    public static readonly string MouseYAxis            = "MouseY";
    public static readonly string PauseAction           = "Pause";
}
