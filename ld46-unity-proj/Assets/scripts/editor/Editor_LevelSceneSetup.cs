﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;


public class Editor_LevelSceneSetup : MonoBehaviour
{
    [MenuItem("Game Tools/Setup Level")]
    private static void SetupSceneLevel()
    {
        if (EditorSceneManager.GetActiveScene().name.ToLower().Contains("level")) // Is level scene
        {
            var config = AssetDatabase.LoadAssetAtPath<LevelSceneSetupConfig>("Assets/config/LevelSceneSetupConfig.asset");
            if (config && config.GoRequiredPrefabs != null && config.GoRequiredPrefabs.Length > 0)
            {
                // Spawn each required prefab that doesn't exist currently in scene
                foreach (var prefab in config.GoRequiredPrefabs)
                {
                    if (GameObject.Find(prefab.name) == null) // Doesn't exist?
                    {
                        PrefabUtility.InstantiatePrefab(prefab);
                        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                        Debug.Log($"Added {prefab.name} prefab to the scene");
                    }
                }
            }
        }
    }
}
