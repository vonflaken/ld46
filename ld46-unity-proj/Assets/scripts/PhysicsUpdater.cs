﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsUpdater : MonoBehaviour
{
    [SerializeField]
    private float _minPhysicsStep = 0.03f; // In seconds


    private void Awake()
    {
        Physics2D.autoSimulation = false;
    }

    private void Update()
    {
        float physicsStep = Mathf.Min(Time.deltaTime, _minPhysicsStep);
        Physics2D.Simulate(physicsStep);
    }
}
