﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Player : MonoBehaviour
{
    [Header("Movement Settings")]

    [SerializeField]
    private Vector2 _maxSpeed = new Vector2(10f, 10f);
    [SerializeField]
    private float _jumpForce = 5f;
    [SerializeField]
    private float _acceleration = 20f;
    [SerializeField]
    private float _gravity = 30f;
    [SerializeField, Range(0f, 1f)]
    private float _floatyness = 0.5f; // How much you can control player direction in air
    [SerializeField, Range(0f, 1f)]
    private float _turnResponsivenessFactor = 0.15f;
    [SerializeField]
    private int _jumpTimeframeSinceFalling = 100; // In ms

    [Header("Physics Settings")]

    [SerializeField]
    private Rigidbody2D _rigidbody = null;
    [SerializeField]
    private BoxCollider2D _mainCollider = null;
    [SerializeField]
    private float _ceilingCheckerDistance = 0.01f;
    [SerializeField]
    private bool _debugDraw = false;

    [Header("Gameplay Settings")]

    [SerializeField]
    private Transform _theContainerCarryPivot = null;
    [SerializeField]
    private float _maxThrowForce = 5f;
    [SerializeField]
    private float _throwForceChargeSpeed = 1f;
    [SerializeField]
    private Vector2 _colliderHeightAndPosYOnDash = Vector2.zero; // x => height | y => centerY

    [Space]
    [SerializeField]
    private AnimationCurve _dashMomentumConfig = null;
    [SerializeField]
    private float _dashSpeedFactor = 3.7f;
    [Space]

    [SerializeField]
    private ParticleSystem _dashParticles = null;
    [SerializeField]
    private int _timeBetweenFootSteps = 350; // In ms
    
    private Timer _jumpTimeframeSinceFallingTimer;
    private Vector2 _colliderSizeNormal, _colliderOffsetNormal = Vector2.zero;
    private Vector2 _colliderSizeDashing, _colliderOffsetDashing = Vector2.zero;
    private int _currentDashDirection = 0;
    private float _timeSinceDashStarted = 0f;    
    private Timer _footStepsTimer;
    private float _currentThrowForce = 0f;
    private Vector2 _lastThrowDirection = Vector2.zero;
    //--- Player state
    private bool    _inAir, 
                    _mustStayCrouch, 
                    _isDashing, 
                    _isDeath, 
                    _isChargingThrow,
                    _isCurrentThrowCanceled = false;
    //--- Animation Settings
    private Animator _animator;
    private static readonly int _apIsWalking = Animator.StringToHash("isWalking"); // *ap* stands for 'animation parameter'
    private static readonly int _apIsCrouched = Animator.StringToHash("isCrouched");
    private static readonly int _apTriggerJump = Animator.StringToHash("triggerJump");
    private static readonly int _apTriggerDash = Animator.StringToHash("triggerDash");
    private static readonly int _apTriggerIdle = Animator.StringToHash("triggerIdle");
    private static readonly int _apTriggerDead = Animator.StringToHash("triggerDead");

    private GameManager _gameMgr = null;
    private ThrowArrowController _throwArrow = null;

    private TheContainer _theContainerScript;
    private TheContainer TheContainerScript
    {
        get {
            if (_theContainerScript == null)
                _theContainerScript = FindObjectOfType<TheContainer>();
            return _theContainerScript;
        }
    }

    private Vector3 CharacterTopExtentPoint {
        get { return _mainCollider.bounds.center + Vector3.up * _mainCollider.bounds.extents.y; }
    }

    private Vector3 GroundCheckerBoxSize {
        get { return new Vector3(_mainCollider.bounds.size.x + 0.1f, 0.1f, 1f); }
    }
    private Vector3 GroundCheckerBoxPosition {
        get { return _mainCollider.bounds.min + Vector3.right * _mainCollider.bounds.extents.x; }
    }

    private Vector2 CurrentVelocity {
        get { return _rigidbody.velocity; }
        set { _rigidbody.velocity = value; }
    }

    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
        _throwArrow = GameObject.FindGameObjectWithTag(Constants.ThrowArrowTag)
            .GetComponent<ThrowArrowController>();
        _throwArrow.SetCallback_RequestPivotWorldPosition = GetCharacterCenter;

        _colliderSizeNormal = _mainCollider.size;
        _colliderOffsetNormal = _mainCollider.offset;
        _colliderSizeDashing = new Vector2(_mainCollider.size.x, _colliderHeightAndPosYOnDash.x);
        _colliderOffsetDashing = new Vector2(_mainCollider.offset.x, _colliderHeightAndPosYOnDash.y);
    }

    private void Start()
    {
        _gameMgr = GameManager.Instance;        
    }
    
    private void Update()
    {
        if (_isDeath)
            return;

        UpdateMovement();
        UpdateThrowTheContainerLogic();
        UpdateAnimations();
    }

    private void UpdateMovement()
    {
        //
        // Pool actions.
        //
        float direction = PoolingDirectionInput();

        if (ShouldDash())
        {
            DoDash((int)direction);
        }

        if (ShouldJump())
        {
            // Add vertical momentum
            DoJump();
        }
                
        if (ShouldMove())
        {
            if (!_isDashing)
            {
                // Add horizontal momentum
                float delta = direction * _acceleration * Time.deltaTime;
                if (_inAir)
                    delta *= _floatyness;
                bool isBackToZero = (CurrentVelocity.x * delta) <= 0f;
                if (!isBackToZero)
                    PushForce(delta, 0f);
                else // Go to zero
                {
                    float blend = 1f - Mathf.Pow(1f - _turnResponsivenessFactor, Time.deltaTime * 60f); // Exponential ease-out
                    float nextTurningVelX = delta + Mathf.Lerp(CurrentVelocity.x, 0f, blend);
                    SetForceX(nextTurningVelX);
                }
            }
            else // Is dashing
            {
                _timeSinceDashStarted += Time.deltaTime;
                float dashProgress = Mathf.Clamp01(_timeSinceDashStarted / GetDashDuration());
                if (dashProgress < 1f)
                {
                    // Process dash movement
                    float nextDashVelX = _maxSpeed.x + _dashMomentumConfig.Evaluate(dashProgress) * _dashSpeedFactor;
                    nextDashVelX *= _currentDashDirection;
                    SetForceX(nextDashVelX);
                }
                else // Dash finished
                    ExitDash();
            }
        }

        // Add gravity
        if (_inAir)
        {
            float fallingDelta = _gravity * Time.deltaTime;
            PushForce(0f, -fallingDelta);
        }

        bool noClampHozVelocity = _isDashing;
        // Flatten current speed
        CurrentVelocity = new Vector3(
            !noClampHozVelocity ? Mathf.Clamp(CurrentVelocity.x, -_maxSpeed.x, _maxSpeed.x) : CurrentVelocity.x,
            Mathf.Clamp(CurrentVelocity.y, -_maxSpeed.y, _maxSpeed.y),
            0f);

        // TODO: Even if now static colliders stop our movement maybe is worth receive a signal and reset horizontal velocity, 
        // eg. we may want to not waste time in revert horizontal velocity to 0 in a direction shift or jump over obstacle with "buffered" velocity.

        // Extrapolate next position for testing purposes
        Vector2 currPosition = _rigidbody.position;
        Vector2 nextPosition = currPosition + CurrentVelocity * Time.deltaTime;
        // Check grounded
        RaycastHit2D[] hits = Physics2D.BoxCastAll(
            GroundCheckerBoxPosition, // Feet pos
            GroundCheckerBoxSize, 
            0f, Vector2.down, 0.2f, 1 << Constants.LayerGround);
        if (hits.Length > 0)
        {
            var horizontalGroundHit = Array.Find(hits, h => h.normal.y > 0f);
            if (CurrentVelocity.y < 0f && horizontalGroundHit) // Is falling?
            {
                // Landed
                _inAir = false;
                SetForceY(0f);
                float groundHeight = horizontalGroundHit.point.y + 0.05f;
                _rigidbody.position = new Vector2(currPosition.x, groundHeight); // Force position on top of the platform
                // Play land sfx
                _gameMgr.PlaySfxOneShot(_gameMgr.SfxLand);
            }
        }
        else if (!_inAir) // Stepped on void?
        {
            _inAir = true;
            _jumpTimeframeSinceFallingTimer.Start(_jumpTimeframeSinceFalling); // Reset
        }
        // Check ceiling        
        RaycastHit2D hit = Physics2D.Raycast(CharacterTopExtentPoint, Vector2.up, _ceilingCheckerDistance, RetrieveCeilingsLayerMask());
        if (hit)
        {
            if (IsCrouched())
                _mustStayCrouch = true;
        }
        else if (IsCrouched() && !_isDashing)
        {
            _mustStayCrouch = false;
            SetMainColliderSizeAndOffset(_colliderSizeNormal, _colliderOffsetNormal);
        }
    }

    private void UpdateThrowTheContainerLogic()
    {
        float rightStickHorizontal = Input.GetAxisRaw(Constants.RS_HAxis);
        float rightStickVertical = Input.GetAxisRaw(Constants.RS_VAxis);
        float mouseMotionX = Input.GetAxis(Constants.MouseXAxis);
        float mouseMotionY = Input.GetAxis(Constants.MouseYAxis);
        bool isRightStickTriggered = Mathf.Abs(rightStickHorizontal) + Mathf.Abs(rightStickVertical) > 0f;
        bool isMouseMoving = Mathf.Abs(mouseMotionX) + Mathf.Abs(mouseMotionY) > 0f;
        bool isThrowActionPressed = Input.GetButton(Constants.ThrowAction);
        if (!isThrowActionPressed) _isCurrentThrowCanceled = false; // Force recover regular throw state after canceled one
        if (!isRightStickTriggered 
            && !isMouseMoving
            && !_isChargingThrow 
            && !isThrowActionPressed) // Keep the aim indicator hidden while neither Player is triggering the proper input nor is charging a throw
        {
            _throwArrow.Hide(true);
            return;
        }

        //
        // At this point, we can update arrow state.
        //
        _throwArrow.Hide(false);
        // Update position
        if (isRightStickTriggered || isMouseMoving || isThrowActionPressed)
        {
            Vector3 arrowPivot = _mainCollider.bounds.center;
            if (isRightStickTriggered) // Controller has priority over mouse
            {
                _lastThrowDirection = new Vector2(rightStickHorizontal, rightStickVertical).normalized;
            }
            else if (isMouseMoving)
            {
                Vector2 headingMouse = (_gameMgr.MainCamera.ScreenToWorldPoint(Input.mousePosition) - arrowPivot).normalized;
                _lastThrowDirection.x = headingMouse.x;
                _lastThrowDirection.y = headingMouse.y;
            }
            
            // Update throwing indicator direction
            _throwArrow.SetDirection(_lastThrowDirection);
        }
        
        if (ShouldChargingThrowTheContainer())
        {
            if (Input.GetButtonDown(Constants.CancelAction))
            {
                _isCurrentThrowCanceled = true;
                ResetThrowState();
            }
            else if (!_isCurrentThrowCanceled)
            {
                _isChargingThrow = true;
                // Update throwing force
                _currentThrowForce = Mathf.Clamp(
                    _currentThrowForce + _throwForceChargeSpeed * Time.deltaTime,
                    0f,
                    _maxThrowForce);
                // Update meter
                _throwArrow.SetPowerMeter(_currentThrowForce, _maxThrowForce);
            }
        }
        else if (ShouldThrowTheContainer())
        {
            if (!_isCurrentThrowCanceled)
                ThrowTheContainer();
        }
        else // Not throwing intent at all
        {
            ResetThrowState();
        }
    }

    private void OnDrawGizmos()
    {
        if (_debugDraw)
        {
            Gizmos.color = Color.green;

            // Show ceiling checker
            Vector3 origin = CharacterTopExtentPoint;
            Gizmos.DrawLine(origin, origin + Vector3.up * _ceilingCheckerDistance);

            Gizmos.color = Color.red;

            // Show ground box checker
            Gizmos.DrawCube(GroundCheckerBoxPosition, GroundCheckerBoxSize);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == Constants.LayerCollectable)
        {
            var colliderParent = other.transform.parent;
            if (colliderParent && colliderParent.CompareTag(Constants.TheContainerTag) && !IsTheContainerBeingCarried() && TheContainerScript.CanGrab())
            {
                // Grab The Container
                GrabTheContainer();
            }
        }
        if (other.gameObject.layer == Constants.LayerHazard)
        {
            Die();
        }
        if (other.gameObject.layer == Constants.LayerGoal 
            && IsTheContainerBeingCarried())
        {
            // Level complete
            GameManager.Instance.GoNextLevel();
            return;
        }
    }

    private void UpdateAnimations()
    {
        // Manage walk
        bool isWalking = IsWalking();
        _animator.SetBool(_apIsWalking, isWalking); // If true then play walking anim
        _animator.SetBool(_apIsCrouched, IsCrouched()); // If both this and above conditions are true then play walking-crouched anim

        if (isWalking && _footStepsTimer.IsDone())
        {
            _gameMgr.PlaySfxOneShot(_gameMgr.SfxSteps);
            _footStepsTimer.Start(_timeBetweenFootSteps);
        }

        // Manage idle
        if (!isWalking && !_inAir && !_isDashing && !_mustStayCrouch) // Is freeze?
            _animator.SetTrigger(_apTriggerIdle); // Play idle anim
        else
            _animator.ResetTrigger(_apTriggerIdle);

        // Turn character based on hoz input direction
        float playerDirection = PoolingDirectionInput();
        if (playerDirection > 0f)
            transform.rotation = Quaternion.identity;
        else if (playerDirection < 0f)
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
    }

    private float PoolingDirectionInput()
    {
        return Input.GetAxisRaw("Horizontal");
    }

    public bool IsWalking()
    {
        return !_inAir && !_isDashing && Mathf.Abs(CurrentVelocity.x) >= 0.08f;
    }

    private bool ShouldJump()
    {
        return CanJump() && Input.GetButtonDown(Constants.JumpAction);
    }

    private bool CanJump()
    {        
        return !_inAir || !_jumpTimeframeSinceFallingTimer.IsDone();
    }

    private void DoJump()
    {
        _inAir = true;
        PushForce(0f, _jumpForce);
        _jumpTimeframeSinceFallingTimer.Reset();
        // Play jump anim
        _animator.SetTrigger(_apTriggerJump);
        // Play jump sfx
        _gameMgr.PlaySfxOneShot(_gameMgr.SfxJump);
        OnPlayerJump();
    }

    private void OnPlayerJump()
    {
        if (_isDashing)
        {
            // Perfom dash to jump transition
            ExitDash();
        }
    }

    private void PushForce(float x, float y)
    {
        CurrentVelocity += new Vector2(x, y);
    }

    private void SetForceX(float x)
    {
        Vector2 temp = CurrentVelocity;
        temp.x = x;
        CurrentVelocity = temp;
    }

    private void SetForceY(float y)
    {
        Vector2 temp = CurrentVelocity;
        temp.y = y;
        CurrentVelocity = temp;
    }

    private bool ShouldMove()
    {
        return true;
    }

    private bool ShouldChargingThrowTheContainer()
    {
        return CanThrowTheContainer() && Input.GetButton(Constants.ThrowAction);
    }

    private bool ShouldThrowTheContainer()
    {
        return CanThrowTheContainer() && Input.GetButtonUp(Constants.ThrowAction);
    }

    private bool CanThrowTheContainer()
    {
        return IsTheContainerBeingCarried();
    }

    private void ThrowTheContainer()
    {
        DoThrowTheContainer(
            _currentThrowForce * _lastThrowDirection.x,
            _currentThrowForce * _lastThrowDirection.y);
    }

    private void DoThrowTheContainer(float forceX, float forceY)
    {
        // Use player momentum plus throw force to impulse The Container
        TheContainerScript.transform.SetParent(null);
        TheContainerScript.ThrowWithVelocity(forceX, forceY);
        // Play throw sfx
        _gameMgr.PlaySfxOneShot(_gameMgr.SfxThrow);
    }

    private void ResetThrowState()
    {
        // Reset throwing vars
        _isChargingThrow = false;
        _currentThrowForce = 0f;
        // Empty meter
        _throwArrow.SetPowerMeter(0f, 0f);
    }

    private bool IsTheContainerBeingCarried()
    {
        return _theContainerCarryPivot.childCount > 0;
    }

    private bool ShouldDash()
    {
        return CanDash() && _gameMgr.Input_WasDashActionTriggered();
    }

    private bool CanDash()
    {
        return !_inAir && !_isDashing;
    }

    private void DoDash(int direction)
    {
        _isDashing = true;
        // Set collider dashing state (crouch)
        SetMainColliderSizeAndOffset(_colliderSizeDashing, _colliderOffsetDashing);
        _currentDashDirection = direction;
        _timeSinceDashStarted = 0f;
        // Drop The Container
        if (IsTheContainerBeingCarried())
        {
            DoThrowTheContainer(0f, 0f);
        }
        // Play dash animation
        _animator.SetTrigger(_apTriggerDash);
        // Play dash particles
        _dashParticles.Play();
        // Play dash sfx
        _gameMgr.PlaySfxOneShot(_gameMgr.SfxDash);
    }

    private void ExitDash()
    {
        _isDashing = false;
        // Set collider normal state back
        if (!_mustStayCrouch)
        {
            SetMainColliderSizeAndOffset(_colliderSizeNormal, _colliderOffsetNormal);
        }
        // Stop dash particles
        _dashParticles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
    }

    private float GetDashDuration()
    {
        return _dashMomentumConfig.keys[_dashMomentumConfig.length - 1].time;
    }

    private int RetrieveCeilingsLayerMask()
    {
        return 1 << Constants.LayerWall
            | 1 << Constants.LayerGround;
    }

    public bool IsCrouched()
    {
        return _mainCollider.size == _colliderSizeDashing;
    }

    private void SetMainColliderSizeAndOffset(Vector2 size, Vector2 offset)
    {
        _mainCollider.size = size;
        _mainCollider.offset = offset;
    }

    private void Die()
    {
        _isDeath = true;
        _mainCollider.enabled = false;
        CurrentVelocity = Vector2.zero; // Freeze character
        // Play dead anim
        _animator.SetTrigger(_apTriggerDead);
        // Play dead sfx
        _gameMgr.PlaySfxOneShot(_gameMgr.SfxDie);
    }

    public void GrabTheContainer()
    {
        TheContainerScript.transform.SetParent(_theContainerCarryPivot);
        TheContainerScript.transform.localPosition = Vector3.zero;
        TheContainerScript.OnGrabbed();
    }

    public Vector2 GetCharacterCenter()
    {
        return _mainCollider.bounds.center;
    }
}
