﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private Camera _camera = null;


    private Transform _followTarget = null;


    private void Awake()
    {
        _followTarget = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void LateUpdate()
    {
        Vector3 nextPos = _camera.transform.position;
        nextPos.x = _followTarget.position.x;
        _camera.transform.position = nextPos;
    }
}
