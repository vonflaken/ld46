﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    private GameObject _levelButtonPrefab = null;
    [SerializeField]
    private RectTransform _levelsGrid = null;


    private void Awake()
    {
        PopulateLevelsGrid();

        var eventSystem = EventSystem.current;
        eventSystem.SetSelectedGameObject(_levelsGrid.GetChild(0).gameObject, new BaseEventData(eventSystem));
    }

    private void PopulateLevelsGrid()
    {
        // Clear current
        int placeholdersCount = _levelsGrid.childCount;
        for (int i = 0; i < placeholdersCount; i++)
        {
            DestroyImmediate(_levelsGrid.GetChild(0).gameObject);
        }

        int levelsCount = SceneManager.sceneCountInBuildSettings - 1;
        // Spawn one button per level
        for (int i = 0; i < levelsCount; i++)
        {
            int levelNum = i + 1;
            var levelButton = Instantiate(_levelButtonPrefab, _levelsGrid);
            // Go to level's scene on click
            levelButton.GetComponentInChildren<Button>().onClick.AddListener(() =>
                SceneManager.LoadScene($"level-{levelNum}"));
            // Set title
            levelButton.GetComponentInChildren<Text>().text = "Level " + levelNum;
        }
    }
}
