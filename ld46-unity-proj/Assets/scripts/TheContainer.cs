﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheContainer : MonoBehaviour
{
    [SerializeField]
    private Collider2D _physicsCollider = null;
    [SerializeField]
    private float _breakThreshold = 0f;

    private Rigidbody2D _rigidbody = null;

    private bool _isBroken = false;
    private Timer _uncollectableTimer; // Controls timespan during which The Container is not collectable


    private void Awake()
    {
        _rigidbody = GetComponentInChildren<Rigidbody2D>();

        ResetVars();
    }

    private void ResetVars()
    {
        _rigidbody.simulated = false;
        _physicsCollider.enabled = false;
        _isBroken = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Check magnitude of impact
        if (collision.relativeVelocity.magnitude > _breakThreshold)
        {
            // Set broken
            _isBroken = true;
            // Play break sfx
            GameManager.Instance.PlaySfxOneShot(GameManager.Instance.SfxGlassBreaks);
        }
    }

    public void ThrowWithVelocity(float forceX, float forceY)
    {
        _rigidbody.simulated = true;
        _physicsCollider.enabled = true;
        _rigidbody.AddForce(new Vector2(forceX, forceY), ForceMode2D.Impulse);
        _uncollectableTimer.Start(100); // This prevent The Container being grabbed right after have been thrown
    }

    public void OnGrabbed()
    {
        // Stop moving
        _rigidbody.simulated = false;
        _physicsCollider.enabled = false;
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = 0f;
    }

    public bool CanGrab()
    {
        return !_isBroken && _uncollectableTimer.IsDone();
    }
}
