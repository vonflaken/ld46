﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Timer
{
    private float _duration; // In sec
    private float _startTime;


    public void Start(int durationMs)
    {
        _duration = durationMs / 1000f;
        _startTime = Time.time;
    }

    public bool IsDone()
    {
        return Time.time >= (_startTime + _duration);
    }

    public void Reset()
    {
        // Invalidate
        _duration = 0f;
    }
}
