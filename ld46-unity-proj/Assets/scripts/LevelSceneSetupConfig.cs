﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSceneSetupConfig", menuName = "Config Assets/Level Scene Setup")]
public class LevelSceneSetupConfig : ScriptableObject
{
    public GameObject[] GoRequiredPrefabs = null;
}
