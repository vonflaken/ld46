﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InGameMenuController : MonoBehaviour
{
    [SerializeField]
    private RectTransform _rootContainer = null;


    private void Awake()
    {
        Hide(true);
    }

    public void OnClick_QuitButton()
    {
        GameManager.Instance.GoMainMenu();
    }

    public void Hide(bool hidden)
    {
        _rootContainer.gameObject.SetActive(!hidden);
    }
}
