﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class ThrowArrowController : MonoBehaviour
{
    [SerializeField]
    private Image _fillablePowerMeter = null;
    [SerializeField]
    private float _radiusAroundTarget = 20f; // In pixels
    [SerializeField]
    private AnimationCurve _fadeOutConfig = null; // y: alpha
    [SerializeField]
    private CanvasGroup _canvasGroup = null;

    private bool _isBeingUsed = false;
    private float _timeSinceStopUsingIt = 0f; // In seconds
    private Vector2 _lastDirSet = Vector2.zero;

    private Func<Vector2> RequestPivotWorldPosition = null;
    public Func<Vector2> SetCallback_RequestPivotWorldPosition { set { RequestPivotWorldPosition = value; } }

    private float FadeOutDuration { get { return _fadeOutConfig.keys[_fadeOutConfig.length - 1].time; } }
    private Vector2 DefaultArrowDir { get { return Vector2.right; } }


    private void Start()
    {
        _canvasGroup.alpha = 0f; // Starts hidden
    }

    private void Update()
    {
        bool isVisible = _canvasGroup.alpha > 0f;
        if (isVisible)
            UpdatePosition();

        if (!_isBeingUsed)
        {
            _timeSinceStopUsingIt += Time.deltaTime;
            if (isVisible)
            {
                // Process fade-out            
                float progress = Mathf.Min(_timeSinceStopUsingIt, FadeOutDuration);
                float newAlpha = _fadeOutConfig.Evaluate(_timeSinceStopUsingIt);
                _canvasGroup.alpha = newAlpha;
            }
        }
    }

    public void Hide(bool hidden)
    {
        if (!hidden)
            _canvasGroup.alpha = 1f; // Show
        else if (_isBeingUsed) // Only reset fade-out if arrow was still in use in previous frame
            _timeSinceStopUsingIt = 0f;
        _isBeingUsed = !hidden;
    }

    public void SetPowerMeter(float power, float maxPower)
    {
        float fillAmount = Mathf.Clamp01(power / maxPower);
        _fillablePowerMeter.fillAmount = fillAmount;
    }

    public void UpdatePosition()
    {
        if (RequestPivotWorldPosition != null)
        {
            Vector2 centerWorldPos = RequestPivotWorldPosition.Invoke();
            Camera cam = GameManager.Instance.MainCamera;
            Vector2 centerScreenPos = cam.WorldToScreenPoint(centerWorldPos);
            Vector2 newPos = centerScreenPos;
            float angle = Vector2.SignedAngle(DefaultArrowDir, _lastDirSet);
            newPos.x += Mathf.Cos(Mathf.Deg2Rad * angle) * _radiusAroundTarget;
            newPos.y += Mathf.Sin(Mathf.Deg2Rad * angle) * _radiusAroundTarget;
            transform.position = newPos; // Set pos        
        }
    }

    public void SetDirection(Vector2 dir)
    {
        _lastDirSet = dir;
        float angle = Vector2.SignedAngle(DefaultArrowDir, dir);
        transform.rotation = Quaternion.Euler(0f, 0f, angle); // Set rot
    }
}
