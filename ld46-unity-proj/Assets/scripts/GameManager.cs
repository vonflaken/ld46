﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    [Header("Sounds")]
    
    [SerializeField]
    private AudioSource _mainSource = null;
    public AudioClip SfxJump;
    public AudioClip SfxDash;
    public AudioClip SfxSteps;
    public AudioClip SfxThrow;
    public AudioClip SfxDie;
    public AudioClip SfxLand;
    public AudioClip SfxGlassBreaks;

    [Header("Prefabs")]

    [SerializeField]
    private GameObject _theContainerPrefab = null;


    public static GameManager Instance;

    public Camera MainCamera { get; private set; }
    public bool IsGamePaused { get; private set; }

    private InGameMenuController _inGameMenuController = null;

    private Player _playerScript = null;
    private int _levelsCount = 0;

    // Input
    private bool _axisAsButtonDownForDashActionFlag = false; 
    private bool _axisForDashActionHeldDown = false;


    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);

        _levelsCount = SceneManager.sceneCountInBuildSettings - 1;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void Update()
    {
        UpdateInput();

        if (IsGameLevel())
        {
            if (Input.GetButtonDown(Constants.ResetAction))
                ResetCurrentLevel();
            else if (Input.GetButtonDown(Constants.PauseAction))
                DoGamePause(!IsGamePaused);
        }
        else if (IsMainMenu())
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }
    }

    private void UpdateInput()
    {
        // Emulate "button down" event with axis for controllers
        _axisAsButtonDownForDashActionFlag = false;
        float dashAxis = Input.GetAxisRaw(Constants.DashAction);
        if (dashAxis == 0)
            _axisForDashActionHeldDown = false;
        else if (dashAxis > 0 && !_axisForDashActionHeldDown)
            _axisAsButtonDownForDashActionFlag = _axisForDashActionHeldDown = true;
    }    

    public void ResetCurrentLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public bool IsGameLevel(Scene? checkScene = null)
    {
        if (checkScene == null) checkScene = SceneManager.GetActiveScene();
        return checkScene.Value.name.ToLower().Contains("level");
    }

    public bool IsMainMenu(Scene? checkScene = null)
    {
        if (checkScene == null) checkScene = SceneManager.GetActiveScene();
        return checkScene.Value.name.ToLower().Contains("MainMenu");
    }

    public void GoNextLevel()
    {
        // Obtain level index from scene naming convention 'level-#'
        int currentLevelIdx = int.Parse(SceneManager.GetActiveScene().name.Split('-')[1]);
        if (currentLevelIdx < _levelsCount) // There are more levels?
        {
            SceneManager.LoadScene($"level-{currentLevelIdx + 1}");
        } 
        else // No more levels
        {
            GoMainMenu();
        }
    }

    public void GoMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    //------------------- Play sfx --------
    public void PlaySfxOneShot(AudioClip sfx)
    {
        _mainSource.PlayOneShot(sfx);
    }

    public bool Input_WasDashActionTriggered()
    {
        return Input.GetButtonDown(Constants.DashAction) || _axisAsButtonDownForDashActionFlag;
    }

    private void OnSceneLoaded(Scene sceneLoaded, LoadSceneMode mode)
    {
        MainCamera = Camera.main;
        _inGameMenuController = FindObjectOfType<InGameMenuController>();

        DoGamePause(false);

        if (IsGameLevel(sceneLoaded))
            OnLevelStart();
    }

    private void OnLevelStart()
    {
        // Spawn The Container
        GameObject.Instantiate(_theContainerPrefab);
        // Let the Player know
        var playerGO = GameObject.FindGameObjectWithTag(Constants.PlayerTag);
        _playerScript = playerGO.GetComponent<Player>();
        _playerScript.GrabTheContainer();
    }

    private void DoGamePause(bool paused)
    {
        IsGamePaused = paused;
        Time.timeScale = paused ? 0f : 1f;
        if (_inGameMenuController)
            _inGameMenuController.Hide(!paused);
    }
}
